use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(Debug)]
pub struct Student {
    id: usize,
    grade: f32,
    selections: Vec<School>
}

impl PartialEq for Student {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for Student {}

impl Hash for Student {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum School {
    Maths,
    Physics
}

type Capacity = usize;
type Accepted = usize;

/// Calculate which student goes to which school on input the results of
/// Panhellenic exams, the students' preferences and the schools' capacities
pub fn assign_students_to_schools<'a>(
    students: Vec<&'a Student>,
    capacity: HashMap<&'a School, Capacity>
) -> HashMap<&'a Student, School> {
    let mut students = students.clone();
    students.sort_by(|a, b| b.grade.partial_cmp(&a.grade).unwrap());

    let mut accepted_map: HashMap<&School, Accepted> = HashMap::new();
    let mut res: HashMap<&Student, School> = HashMap::new();

    for student in students {
        for school in &student.selections {
            let accepted = accepted_map.entry(&school).or_insert(0);
            // assume no miss in capacity
            if *accepted < capacity[&school] {
                *accepted += 1;
                res.insert(&student, *school);
                break;
            }
        }
    }

    res
}

#[test]
fn assigns_well() {
    let george = Student {
        id: 0,
        grade: 19.5,
        selections: vec![School::Maths]
    };

    let maria = Student {
        id: 1,
        grade: 20.0,
        selections: vec![School::Maths, School::Physics]
    };

    let kate = Student {
        id: 2,
        grade: 18.0,
        selections: vec![School::Physics]
    };

    let billy = Student {
        id: 3,
        grade: 15.0,
        selections: vec![School::Physics, School::Maths]
    };

    let students = vec![&george, &maria, &kate, &billy];

    let mut capacity = HashMap::new();
    capacity.insert(&School::Maths, 2);
    capacity.insert(&School::Physics, 1);

    let mut expected = HashMap::new();
    expected.insert(&george, School::Maths);
    expected.insert(&maria, School::Maths);
    expected.insert(&kate, School::Physics);

    assert_eq!(assign_students_to_schools(students, capacity), expected);
}
